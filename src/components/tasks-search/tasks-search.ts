import { computed, watch } from 'vue'
import useTasksController from '@/middleware/controllers/useTasksController'
import useTasksList from '@/store/tasks'
import { TaskFilters } from '@/models/task.model'
import { debounce } from '@/helpers/utils'
import { useTaskFilters, useTaskPagination, useTaskFiltersUpdate } from '@/components/tasks-search/tasks-params'

export default function useTasks() {
  const { tasksList, tasksCount } = useTasksList()
  const { getTasks, clearTasksCache } = useTasksController()

  const {
    updateKeywords,
    updateBudgetGreaterEqual,
    updateBudgetLowerEqual,
    updatePlatforms,
    updatePage
  } = useTaskFiltersUpdate()


  const pagination = useTaskPagination()
  const filters = useTaskFilters()
  const selectedPage = computed(() => pagination.skip / pagination.limit + 1)

  const getTasksByNewFilters = (filters: TaskFilters) => {
    clearTasksCache()
    updatePage(1)
    getTasks(pagination, filters)
  }

  watch(filters, debounce(getTasksByNewFilters, 300))
  watch(pagination, (newValue) => getTasks(newValue, filters))

  getTasks(pagination, filters)

  return {
    tasksList,
    tasksCount,
    selectedPage,
    pagination,
    filters,
    updateKeywords,
    updateBudgetLowerEqual,
    updateBudgetGreaterEqual,
    updatePlatforms,
    updatePage,
  }
}