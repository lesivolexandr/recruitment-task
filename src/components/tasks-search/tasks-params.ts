import { computed, reactive } from 'vue'
import { tasksPerPage } from '@/constants/tasks'
import { useUpdateUrlParams, useArrayQueryParam, useNumericQueryParam } from '@/helpers/route'
import { Pagination } from '@/models/pagination.model'

export const useTaskPagination = (): Pagination => {
  const pageParam = useNumericQueryParam('page')

  const pagination: Pagination = reactive({
    skip: computed(() => ((pageParam.value || 1) - 1) * tasksPerPage),
    limit: tasksPerPage,
  })

  return pagination
}

export const useTaskFilters = () => {
  const filters = {
    keywords: useArrayQueryParam('keywords'),
    platforms: useArrayQueryParam('platforms'),
    budgetLowerEqual: useNumericQueryParam('budgetLowerEqual'),
    budgetGreaterEqual: useNumericQueryParam('budgetGreaterEqual'),
  }
  const reactiveFilters = reactive(filters)
  return reactiveFilters
}

const _separateKeywords = (keywordsString: string) => {
  // regEpx replaces all punctuation marks with white spaces and turns any sequence of white spaces into single white space
  const keywords = keywordsString
    .replace(/\p{P}/gu, ' ')
    .replace(/\s+/g, ' ')
    .trim()
    .split(' ')

  return keywords
}

const _useUpdateKeywords = () => {
  const updateUrlParams = useUpdateUrlParams()

  return (keywordsString: string) => {
    const keywords = _separateKeywords(keywordsString)
    const comaSeparatedKeywords = keywords.join(',')

    updateUrlParams({ keywords: comaSeparatedKeywords })
    return keywords
  }
}

const _useUpdateBudgetGreaterEqual = () => {
  const updateUrlParams = useUpdateUrlParams()

  return (budgetGreaterEqual: number) => {
    const newValue = budgetGreaterEqual || budgetGreaterEqual === 0 ? String(Math.max(budgetGreaterEqual, 0)) : null
    const newParams = {
      budgetGreaterEqual: newValue,
    }

    updateUrlParams(newParams)
  }
}

const _useUpdateBudgetLowerEqual = () => {
  const updateUrlParams = useUpdateUrlParams()

  return (budgetLowerEqual: number) => {
    const newValue = budgetLowerEqual || budgetLowerEqual === 0 ? String(budgetLowerEqual) : null
    const newParams = {
      budgetLowerEqual: newValue
    }

    updateUrlParams(newParams)
  }
}

const _useUpdatePlatforms = () => {
  const updateUrlParams = useUpdateUrlParams()
  return (platforms: string[]) => {
    const platformsString = platforms.join(',')
    updateUrlParams({ platforms: platformsString })
  } 
}

const _useUpdatePage = () => {
  const updateUrlParams = useUpdateUrlParams()
  return (pageNumber: number) => {
    updateUrlParams({ page: String(pageNumber) })
  }
}

export const useTaskFiltersUpdate = () => {
  const updateKeywords = _useUpdateKeywords()
  const updateBudgetGreaterEqual = _useUpdateBudgetGreaterEqual()
  const updateBudgetLowerEqual = _useUpdateBudgetLowerEqual()
  const updatePlatforms = _useUpdatePlatforms()
  const updatePage = _useUpdatePage()

  return {
    updateKeywords,
    updateBudgetGreaterEqual,
    updateBudgetLowerEqual,
    updatePlatforms,
    updatePage,
  }
}