import { Ref, ref } from 'vue'
import { TaskPublicInfo } from '@/models/task.model'


const tasksList: Ref<TaskPublicInfo[]> = ref([])
const tasksCount: Ref<number> = ref(0)

export default function useTasksList() {
  const setTasks = (data: TaskPublicInfo[]) => tasksList.value = data
  const setTasksCount = (newTasksCount: number) => tasksCount.value = newTasksCount

  return {
    tasksList,
    tasksCount,
    setTasks,
    setTasksCount,
  }
}
