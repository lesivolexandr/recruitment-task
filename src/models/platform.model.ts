export enum Platform {
  INSTAGRAM = 'INSTAGRAM',
  FACEBOOK = 'FACEBOOK',
  YOUTUBE = 'YOUTUBE',
  TWITCH = 'TWITCH',
}

export interface PlatformLabeled {
  label: string;
  value: Platform;
}
