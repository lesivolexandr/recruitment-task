import { CurrencyValue } from "./money.model";
import { Platform } from "./platform.model";

export interface TaskPublicInfo {
  id: string;
  addedTime: string;
  title: string;
  description: string;
  budget: CurrencyValue;
  proposalCount: number;
  platforms: Platform[];
  proposalAlreadySent: boolean;
  addedByMe: boolean;
  hasContractCreated: boolean;
  hasContractAccepted: boolean;
}

export interface TaskFilters {
  keywords?: string[];
  budgetLowerEqual?: number;
  budgetGreaterEqual?: number;
  platforms?: string[];
}