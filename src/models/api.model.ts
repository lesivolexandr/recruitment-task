import { Pagination } from './pagination.model';
import { TaskFilters } from './task.model';

export type QueryParams = TaskFilters & Pagination
