import { Platform, PlatformLabeled } from '@/models/platform.model'

export const tasksPerPage = 3
export const platforms: PlatformLabeled[] = [
  {
    label: 'Instagram',
    value: Platform.INSTAGRAM
  }, {
    label: 'Facebook',
    value: Platform.FACEBOOK
  }, {
    label: 'Youtube',
    value: Platform.YOUTUBE
  }, {
    label: 'Twitch',
    value: Platform.TWITCH
  }
];
