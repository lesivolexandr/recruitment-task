import { ref, watch } from 'vue'
import { LocationQueryValue, useRoute, useRouter } from 'vue-router'

const _useUrlQueryParam = (paramName: string) => {
  const route = useRoute()
  const paramValue = route.query[paramName]

  if (Array.isArray(paramValue)) {
    return paramValue.filter(Boolean).map(String)
  }
  return paramValue ? String(paramValue) : undefined
}

const _numericQueryParamTransform = (value?: LocationQueryValue | LocationQueryValue[]) => {
  if (Array.isArray(value)) {
    return undefined
  }
  if (value === null) {
    return undefined
  }
  const numericValue = Number(value)
  return Number.isNaN(numericValue) ? undefined : numericValue
}

const _arrayQueryParamTransform = (value?: LocationQueryValue | LocationQueryValue[]) => {
  if (Array.isArray(value)) {
    return value.map(String)
  }
  if (!value) {
    return undefined
  }
  return value.split(',')
}

export const useNumericQueryParam = (paramName: string) => {
  const paramValue = _useUrlQueryParam(paramName)
  const numericValue = _numericQueryParamTransform(paramValue)
  const param = ref(numericValue)

  const route = useRoute()
  watch(() => route.query[paramName], (newValue) => param.value = _numericQueryParamTransform(newValue))

  return param
}

export const useArrayQueryParam = (paramName: string) => {
  const paramValue = _useUrlQueryParam(paramName)
  const arrayValue = _arrayQueryParamTransform(paramValue)
  const param = ref(arrayValue)

  const route = useRoute()
  watch(() => route.query[paramName], (newValue) => param.value = _arrayQueryParamTransform(newValue))

  return param
}

export const useUpdateUrlParams = () => {
  const route = useRoute()
  const router = useRouter()

  return (newParams: Record<string, string | null>) => {
    const query: Record<string, LocationQueryValue | LocationQueryValue[]> = {
      ...route.query,
      ...newParams,
    }

    router.push({ query })
  }
}