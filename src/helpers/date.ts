import { inject, provide } from 'vue'
import { RELATIVE_DATE_FORMATTER_KEY } from '@/constants/injection-keys'

const _second = 1000
const _minute = _second * 60
const _hour = _minute * 60
const _day = _hour * 24
const _year = Math.round(_day * 365.2422) // this is the exact duration of the year according to NASA https://www.grc.nasa.gov/www/k-12/Numbers/Math/Mathematical_Thinking/calendar_calculations.htm
const _month = _year / 12

const _minutesAgo = (date: Date) => {
  const dateDiff = Date.now() - date.valueOf()
  const minutesAgo = Math.round(dateDiff / _minute)
  
  return minutesAgo
}

const _hoursAgo = (date: Date) => {
  const dateDiff = Date.now() - date.valueOf()
  const hoursAgo = Math.round(dateDiff / _hour)

  return hoursAgo
}

const _daysAgo = (date: Date) => {
  const dateDiff = Date.now() - date.valueOf()
  const daysAgo = Math.round(dateDiff / _day)

  return daysAgo
}

const _monthsAgo = (date: Date) => {
  const dateDiff = Date.now() - date.valueOf()
  const monthsAgo = Math.round(dateDiff / _month)

  return monthsAgo
}

const _yearsAgo = (date: Date) => {
  const dateDiff = Date.now() - date.valueOf()
  const yearsAgo = Math.round(dateDiff / _year)

  return yearsAgo
}

const stringifyDateUnits = (value: number, unit: 'minute' | 'hour' | 'day' | 'month' | 'year') => {
  if (value === 1) {
    return `${value} ${unit} ago`
  }
  return `${value} ${unit}s ago`
}

const formatDateRelative: DateFormatter = (date: number | string | Date) => {
  const dateObj = new Date(date)
  const minutesAgo = _minutesAgo(dateObj)
  if (minutesAgo < 60) {
    return stringifyDateUnits(minutesAgo, 'minute')
  }

  const hoursAgo = _hoursAgo(dateObj)
  if (hoursAgo < 24) {
    return stringifyDateUnits(hoursAgo, 'hour')
  }

  const daysAgo = _daysAgo(dateObj)
  if (daysAgo < 31) {
    return stringifyDateUnits(daysAgo, 'day')
  }

  const monthsAgo = _monthsAgo(dateObj)
  if (monthsAgo <= 12) {
    return stringifyDateUnits(monthsAgo, 'month')
  }

  const yearsAgo = _yearsAgo(dateObj)
  return stringifyDateUnits(yearsAgo, 'year')
}

export interface DateFormatter {
  (date: number | string | Date): string;
}

export const provideRelativeDateFormatter = (formatDate: DateFormatter = formatDateRelative) => {
  return provide(RELATIVE_DATE_FORMATTER_KEY, formatDate)
}

export const injectRelativeDateFormatter = () => {
  const dateFormatter = inject<DateFormatter>(RELATIVE_DATE_FORMATTER_KEY)
  if (!dateFormatter) {
    throw new Error('You have not provided date formatter')
  }
  return dateFormatter
}