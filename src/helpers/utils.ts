/* eslint-disable @typescript-eslint/no-explicit-any */
export function debounce<T extends ((...args: any[]) => unknown)>(func: T, wait: number): T {
  let timeout: number | null = null
  return function(this: unknown, ...args) {
    const context = this as any
    if (timeout) {
      clearTimeout(timeout)
      timeout = null
    }
    timeout = setTimeout(() => func.apply(context, args), wait)
  } as T
}