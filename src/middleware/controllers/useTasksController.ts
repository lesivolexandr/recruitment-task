import { tasksPerPage } from '@/constants/tasks'
import { QueryParams } from '@/models/api.model'
import { Pagination } from '@/models/pagination.model'
import { TaskFilters, TaskPublicInfo } from '@/models/task.model'
import api, { paramsToQueryString } from '@/services/api'
import useTasksList from '@/store/tasks'

const tasksCache: Map<number, TaskPublicInfo[]> = new Map()

const { setTasks, setTasksCount } = useTasksList()

const _getCurrentPageTasks = async (pagination: Pagination, filters?: TaskFilters) => {
  const params: QueryParams = {
    ...filters,
    ...pagination
  }

  const pageNumber = pagination.skip / pagination.limit + 1
  const tasksFromStore = tasksCache.get(pageNumber)


  if (!tasksFromStore) {
    await api.get('tasks', { params, paramsSerializer: paramsToQueryString }).then(response => {
      setTasks(response.data.tasks)
      tasksCache.set(pageNumber, response.data.tasks)
      setTasksCount(response.data.count + (pageNumber - 1) * tasksPerPage)
    })
  }

  if (tasksFromStore) {
    setTasks(tasksFromStore)
  }
}

const _getNextPageTasks = async (pagination: Pagination, filters?: TaskFilters) => {
  const params: QueryParams = {
    ...filters,
    ...pagination,
    skip: pagination.skip + tasksPerPage
  }

  const pageNumber = pagination.skip / pagination.limit + 2

  const nextPageTasks = tasksCache.get(pageNumber)
  if (!nextPageTasks) {
    await api.get('tasks', { params, paramsSerializer: paramsToQueryString }).then(response => {
      tasksCache.set(pageNumber, response.data.tasks)
    })
  }
}

const getTasks = async (pagination: Pagination, filters?: TaskFilters) => {
  await _getCurrentPageTasks(pagination, filters)
  return _getNextPageTasks(pagination, filters)
}

export default function useTasksController() {
  const clearTasksCache = () => tasksCache.clear()

  return {
    getTasks,
    clearTasksCache,
  }
}
